const { list, expected } = require('./data');
// basic triple loop through
let foo = (list) => {
  const LIST_LENGTH = list.length;
  for(let i = 0; i < LIST_LENGTH; i++) {
    for(let j = i+1; j < LIST_LENGTH; j++) {
      for(let k = j+1; k < LIST_LENGTH; k++) {
        const a = list[i];
        const b = list[j];
        const c = list[k];
        const sum = a+b+c;
        if(sum === 2020) {
          return a*b*c;
        }
      }
    }
  }
}
// trying to jazz it up a bit
let bar = (list) => {
  const listlen = list.length;
  let i = 0;
  let j = 1;
  let k = 2;
  let sum = list[i] + list[j] + list[k];
  while(sum !== 2020 && i < listlen - 2) {
    if(j >= listlen - 2) {
      j = (++i) + 1;
      k = j + 1;
    } else {
      if(k >= listlen - 1) {
        k = (++j) + 1; 
      } else {
        k++;
      }
    }
    sum = list[i] + list[j] + list[k];
  }
  return list[i] * list[j] * list[k];
};

let foobar = (list) => {
  const listlen = list.length;
  const terms = 3;
  const cursors = [ 0,1,2 ];

  let sum = () => cursors.reduce((total, cursor) => total + list[cursor], 0);

  while(sum() !== 2020 && cursors[0] < listlen - 2) {
    // iterate through cursors from the second one
    let activeCursorIndex = 1;
    while(activeCursorIndex < terms){
      const activeCursor = cursors[activeCursorIndex];
      const offset = terms - activeCursorIndex;
      // base case is that it's the last cursor and
      // it's not at offset limit
      // in which case just increment
      if(activeCursorIndex == terms - 1 && activeCursor < listlen - offset) {
        cursors[activeCursorIndex] = activeCursor + 1;
      } else {
        // else increment previous cursor and subsequent cursors
        const lastCursorIndex = activeCursorIndex - 1;
        const newCursorBaseValue = cursors[lastCursorIndex] + 1;
        for(let i = 0; i < terms; i++) {
          cursors[lastCursorIndex + i] = newCursorBaseValue + i;  
        }
      }
      activeCursorIndex++;
    }
  }
  return cursors.reduce((product, cursor) => product * list[cursor], 1);
};

let baz = (list, terms) => {
  const listlen = list.length;
  if(terms < 1 || terms > list.length) { return -1 };
  const cursors = new Array(terms).fill(0).map((_, index) => index);

  let sum = () => cursors.reduce((total, cursor) => total + list[cursor], 0);

  while(sum() !== 2020 && cursors[0] < listlen - 2) {
    let activeCursorIndex = 1;
    while(activeCursorIndex < terms){
      const activeCursor = cursors[activeCursorIndex];
      const offset = terms - activeCursorIndex;
      if(activeCursorIndex == terms - 1 && activeCursor < listlen - offset) {
        cursors[activeCursorIndex] = activeCursor + 1;
      } else {
        const lastCursorIndex = activeCursorIndex - 1;
        const newCursorBaseValue = cursors[lastCursorIndex] + 1;
        for(let i = 0; i < terms; i++) {
          cursors[lastCursorIndex + i] = newCursorBaseValue + i;  
        }
      }
      activeCursorIndex++;
    }
  }
  return cursors.reduce((product, cursor) => product * list[cursor], 1);
};

const functions = [foo, bar, foobar, baz];
console.log('expected with 3 terms', expected['3'])
functions.forEach((fn, index) => {
  console.log('actual with 3', fn(list, 3));
})
