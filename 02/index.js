const l = [
  '1-3 a: abcde',
  '1-3 b: cdefg',
  '2-9 c: ccccccccc'
];
const part1 = l.reduce((total, item) => {
  const [rule, pass] = item.split(": ");
  const [amt, letter] = rule.split(" ");
  const [min, max] = amt.split("-");
  let instances = pass.split('').reduce((num, passl) => {
    return passl == letter
      ? num + 1
      : num
  }, 0)
  return instances >= min && instances <= max 
    ? total + 1
    : total;
}, 0)
const part2 = l.reduce((areValid, item) => {
  const [rule, pass] = item.split(": ");
  const [amt, letter] = rule.split(" ");
  const [a, b] = amt.split("-").map(i => i -1);

  return pass[a] == letter && pass[b] != letter 
    || pass[b] == letter && pass[a] != letter
    ? areValid + 1
    : areValid;
}, 0)
