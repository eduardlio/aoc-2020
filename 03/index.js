const data = require('./data')

let cleanData = source => source.trim().split("\n");

let example = cleanData(`
..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
`);

const l = cleanData(data);

const refactoredPart1 = (list, dx, dy) => {
	let trees = 0;
	const isTree = (char) => char === '#';
	let x = 0;
	let y = 0;
	while(y < list.length){
		const row = list[y];
		const mod = x % row.length;
		const itemAtPosition = list[y][mod];
		if(isTree(itemAtPosition)){
			trees++;
		}
		x+=dx;
		y+=dy;
	}
	return trees;
}
console.log(refactoredPart1(l, 3, 1));
const refactoredPart2 = (list, slopes) => {
  return slopes.reduce((total, slope) => {
    return refactoredPart1(list, ...slope) * total;
  }, 1)
}
const slopes = [
  [1, 1],
  [3, 1],
  [5, 1], 
  [7, 1], 
  [1, 2],
]
console.log(refactoredPart2(l, slopes));
