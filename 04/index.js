const original = require("./data");
const { cleanData } = require("../utils");

const clean = (input) =>
    input
        .trim()
        .split("\n\n")
        .map((i) => i.replace(/\n/g, " "));
const data = clean(original);
const example = clean(`
ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in
`);

const intInRange = (n, to, from) =>
    parseInt(n).toString() != "NaN" && to <= parseInt(n) && parseInt(n) <= from;
const requiredFields = {
    byr: (a) => intInRange(a, 1920, 2002),
    iyr: (a) => intInRange(a, 2010, 2020),
    eyr: (a) => intInRange(a, 2020, 2030),
    hgt: (a) => {
        const metric = /cm|in/g.exec(a) || '';
		const num = parseInt(/\d+/g.exec(a)[0] || -1);
		switch(metric[0]) {
			case 'cm': return intInRange(num, 150, 193)
			case 'in': return intInRange(num, 59, 76)
			default: return false
		}
    },
    hcl: (a) => {
        const reg = /(?<h>#)(?<letters>[a-f|\d]+)/g;
        const result = reg.exec(a);
        return result != null && result.groups.letters.length === 6;
    },
    ecl: (a) => {
        const cols = "amb blu brn gry grn hzl oth".split(" ");
        return cols.indexOf(a) > -1;
    },
    pid: (a) => {
        const reg = /\d+/g;
        return reg.exec(a)[0].length === 9;
    },
    cid: (a) => true,
};
const requiredFields1 = Object.keys(requiredFields);
const part1 = (list) => {
    return list.reduce((valid, passport) => {
        const data = {};
        passport.split(" ").forEach((pair) => {
            const [key, value] = pair.trim().split(":");
            data[key] = value;
        });

        const allFieldsValid = requiredFields.reduce((ok, field) => {
            return (!!data[field] || field === "cid") && ok;
        }, true);

        return allFieldsValid ? valid + 1 : valid;
    }, 0);
};
// console.log(part1(example)); // should return  2
// console.log(part1(data));

const part2 = (list) => {
    return list.reduce((valid, passport) => {
        const data = {};
        passport.split(" ").forEach((pair) => {
            const [key, value] = pair.trim().split(":");
            data[key] = value;
        });
        const allFieldsValid = Object.keys(requiredFields).reduce(
            (ok, field) => {
                return !!data[field]
                    ? requiredFields[field](data[field]) && ok
                    : field === "cid" && ok;
            },
            true
        );
        return allFieldsValid ? valid + 1 : valid;
    }, 0);
};
const bad = clean(`
eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007
`);
const good = clean(`
pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719`);
// console.log(part2(good)); // should return 4
// console.log(part2(bad)); // should return 0
// console.log(part2(data));
