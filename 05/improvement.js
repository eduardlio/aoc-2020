const original = require("./data");
const { cleanData } = require("../utils");
const data = cleanData(original);
const getIds = (seats) => {
	// this part1 solution is basically a javascript version of
	// @bradleysigma's implementation of this puzzle
    return seats.map((seat) => {
		// turn (F,L) -> 0, (B,R) -> 1
		// makes it a binary str of row
		// parseInt(str, radix)
        const row = parseInt(
            seat.slice(0, 7).replace(/F/g, "0").replace(/B/g, "1"),
            2
		);
        const col = parseInt(
            seat.slice(7).replace(/L/g, "0").replace(/R/g, "1"),
            2
        );
        return 8 * row + col
    });
};
const part1 = () => Math.max(...getIds(data))
const part2 = () => {
    const sorted = getIds(data).sort();
    let last = sorted[0];
    let found = -1;
    for (let id of sorted) {
        if (id - last > 1) {
            found = id - 1;
            break;
        } else {
            last = id;
        }
    }
    return found;
};
console.log(part1())
console.log(part2());
