const original = require("./data");
const { cleanData } = require("../utils");

const data = cleanData(original);

const seatId = (row, col) => (row * 8) + col;

const part1 = (seat) => {
	let r_head = 0;
	let r_tail = 127;
	let c_head = 0;
	let c_tail = 7;
	for(let i = 0; i < 7;i++) {
		const letter = seat[i]
		const diff = (r_tail - r_head)/2
		if(letter ==='F'){
			r_tail = Math.floor(r_tail-diff)
		} else {
			r_head = Math.ceil(r_tail-diff);
		}
	}
	const row = r_head;
	const cols = seat.split("").slice(-3).join("")
	for(let i = 0; i < 3;i++) {
		const letter = cols[i]
		const diff = (c_tail - c_head)/2
		if(letter ==='L'){
			c_tail = Math.floor(c_tail-diff)
		} else {
			c_head = Math.ceil(c_tail-diff);
		}
	}
	const col = c_head;
	return seatId(row, col);
};
const max = data.reduce((highest, seat) => {
	id = part1(seat);
	if(id > highest) return id;
	else return highest;
}, 0)

console.log(max);

const part2 = (list) => {
	const ids = list.map((seat) => {
		return part1(seat)
	});
	const sorted = ids.sort()
	let last = sorted[0];
	let found = -1
	for(let id of sorted) {
		if((id - last) > 1) {
			found = id-1
			break;
		} else {
			last = id;
		}
	}
	return found;
};
console.log(part2(data));
