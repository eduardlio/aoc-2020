
const original = require('./data');
const { cleanData } = require('../utils');
const clean = (thing) => thing.trim().split("\n\n").map(i => i.trim().split("\n"));

const data = clean(original);
const example = clean(`
abc

a
b
c

ab
ac

a
a
a
a

b
`);

const part1 = (list) => {
	const qns = list.reduce((total, group) => {
		const state = {}
		group.forEach(response => {
			for(letter of response) {
				state[letter] = 1;
			}
		})
		return total + Object.keys(state).length ;
	}, 0)
	return qns;
};
console.log(part1(example));
console.log(part1(data));


const part2 = (list) => {
	const qns = list.reduce((total, group) => {
		const state = {}
		group.forEach(response => {
			for(letter of response) {
				if(state[letter]) {
					state[letter] = state[letter] + 1;
				} else {
					state[letter] = 1;
				}
			}
		})
		const allAnswered = Object.keys(state).reduce((answered, qn) => {
			return state[qn] == group.length ? answered + 1 : answered
		}, 0)
		return total + allAnswered
	}, 0)
	return qns;
};
// console.log(part2(example));
console.log(part2(data));

