const original = require("./data");
const { cleanData } = require("../utils");

const data = cleanData(original);
const example = cleanData(`
light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.
`);
const example2 = cleanData(`
shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.`);

const part1 = (list) => {
  const state = {}
  list.forEach(bag => {
    const [a, b] = bag.split(" contain ");
    const colour = a.split(" ").slice(0, 2).join(" ");
    const contents = b.split(", ")
      .map(b => {
        const str = b.replace(/bag(s)?(.)?/g, "").trim();
        if(str.split(" ")[0] === 'no'){
          return null
        } else {
          return [parseInt(str[0]), str.split(" ").slice(1).join(" ")]
        }
      })
  })
}
const part2 = (list) => {
}


console.log(part1(example));
