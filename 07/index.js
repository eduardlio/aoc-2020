const original = require("./data");
const { cleanData } = require("../utils");

const data = cleanData(original);
const example = cleanData(`
light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.
`);
const example2 = cleanData(`
shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.`);

const part1 = (list) => {
    const state = {};
    list.forEach((bag) => {
        const [col, cont] = bag.split("contain");
        const colour = col.split(" ").slice(0, 2).join(" ");
        const contents = cont
            .split("bag")
            .map((i) => i.trim())
            .slice(0, -1)
            .map((b) => {
                const match = b.split(/\d/g)[1];
                // console.log(match);
                if (match) {
                    return match.trim();
                } else {
                    return match;
                }
            });
        state[colour] = contents;
    });
    let cts = [];
    for (let bag of Object.keys(state)) {
        if (state[bag].findIndex((i) => i == "shiny gold") >= 0) {
            cts.push(bag);
        }
    }
    const checked = {};
    const found = {};
    let c = 0;

    const search = (contains, bag) => {
        if (contains.length === 0) {
            return;
        }
        console.log("looking for", bag);
        c++;
        let next = [];
        for (let [key, val] of Object.entries(state)) {
            // if it's not already checked
            if (!checked[key]) {
                // check contents of bag
                if (val.includes(bag)) {
                    // the found bag is one of them
                    found[key] = 1;
                    next.push(key);
                }
            }
        }
        const nextBag = contains.shift();
        return search([...contains, ...next], nextBag);
    };
    search(cts, "shiny gold");
    console.log(c);
    return Object.keys(found).length;
};
// console.log(part1(example));
// console.log(part1(example2));
// console.log(part1(data));

const part2 = (list) => {
    const state = {};
    list.forEach((bag) => {
        const [col, cont] = bag.split(/bags contain/g);
        const colour = col.split(" ").slice(0, 2).join(" ");
        const contents = cont.split(", ").map((b) => {
            const str = b.replace(/bag(s)?(.)?/g, "").trim();
            if(str.split(" ")[0] === 'no'){
                return null
            } else {
                return [parseInt(str[0]), str.split(" ").slice(1).join(" ")]
            }
        });
        state[colour] = contents;
    });
    const getBagsWithin = (bag) => {
        const contents = state[bag];
        if(!contents[0]){
            return 0;
        }
        return contents.reduce((sum, item) => {
            return sum + item[0] + item[0] * getBagsWithin(item[1])
        }, 0)
    }
    return getBagsWithin("shiny gold");
};
// console.log(part2(example2));
console.log(part2(data));
