const original = require("./data");
const { cleanData } = require("../utils");

const data = cleanData(original);
const example = cleanData(`
nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6
`);

const part1 = (list) => {
    let cursor = 0;
    let acc = 0;
    let stop = false;
    while (!stop  && cursor < list.length) {
        const instruction = list[cursor];
        // console.log(instruction)
        let [action, amt] = instruction.split(" ");
        amt = parseInt(amt);
        switch (action) {
            case "nop":
                list[cursor] = "x" + instruction;
                cursor++;
                break;
            case "acc":
                list[cursor] = "x" + instruction;
                cursor++;
                acc += amt;
                break;
            case "jmp":
                list[cursor] = "x" + instruction;
                cursor += amt;
                break;
            default:
                stop = true;
                console.log('acc before exit', acc)
                acc = -1
				break;
        }
    }
    return acc;
};
console.log(part1(example));
// console.log(part1(data));

const part2 = (list) => {
    let tester = 0;
    while(tester < list.length) {
        const instruction = list[tester];
        let [action, amt] = instruction.split(" ");
        if(action !== 'acc') {
            const changed = [...list] 
            changed[tester] = action === 'nop' 
                ? 'jmp ' + amt
                : 'nop ' + amt
            let result = part1(changed);
            if(result > 0) {
                return result;
            }
        }
        tester++;
    }
};
// console.log(part2(example));
// console.log(part2(data));
