const original = require("./data");
const { cleanData } = require("../utils");

const clean = (str) =>
    str
        .trim()
        .split("\n")
        .map((i) => parseInt(i.trim()));
const data = clean(original);
const example = clean(`
35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576
`);

const part1 = (list, bufferlen) => {
    let head = 0;
    let tail = bufferlen;
    let found = false;
    let odd = -1;
    let cc = 0;
    while (!found && cc < 100) {
        const target = list[tail];
        let innerHead = head;
        let innerTail = head + 1;
        const searchEnd = tail - 1;
        let summed = false;
        while (innerHead < searchEnd - 1 && !summed) {
            let a = list[innerHead];
            let b = list[innerTail];
            if (target === a + b) {
                summed = true;
            } else {
                if (innerTail >= searchEnd) {
                    innerTail = ++innerHead;
                } else {
                    innerTail++;
                }
            }
        }
        if (!summed) {
            odd = target;
            found = true;
        }
        head++;
        tail++;
    }
    return odd;
};
// console.log(part1(example, 5));
// console.log(part1(data, 25));

const part2 = (list, target) => {
    let head = 0;
    let tail = 1;
	let min = Number.MIN_VALUE;
	let max = Number.MAX_VALUE;
	let sum = 0;
    while (head < list.length) {
		sum = 0;
		min = list[head];
		max = list[head];
		while(sum < target) {
			let a = list[tail++];
			sum += a;
			if(a < min) {
				min = a;
			}
			if(max < a) {
				max = a;
			}
		}
		if(sum === target) {
			break;
		}
		tail = ++head;
	}
	return min + max;
};
// console.log(part2(example, part1(example, 5)));
console.log(part2(data, part1(data, 25)));
