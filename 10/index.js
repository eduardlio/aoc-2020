const original = require("./data");
const { cleanData } = require("../utils");
const clean = (a) =>
    a
        .trim()
        .split("\n")
        .map((i) => parseInt(i))
        .sort((b, a) => b - a);

const data = clean(original);
const example = clean(`
16
10
15
5
1
11
7
19
6
12
4
`);
const example2 = clean(`
28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3
`);

const part1 = (list) => {
    const state = { 1: 0, 2: 0, 3: 1 };
    const firstDiff = list[0] - 0;
    state[firstDiff] = state[firstDiff] + 1;
    let i = 0;
    while (i < list.length) {
        // console.log(list[i+1], list[i])
        const diff = list[i + 1] - list[i];
        state[diff] = state[diff] + 1;
        i++;
        // console.log(state)
    }
    console.log(state[1], state[3]);
    return state[1] * state[3];
};
// console.log(part1(example));
// console.log(part1(example2));
// console.log(part1(data));

const part2 = (list) => {
    let buf = 3;
    let head = 0;
    let tail = 0;
    const combos = {};
    let byA = {};
    let largestFromPrev = -1;
    const biggest = list[list.length - 1];
    list = [0, ...list, biggest + 3];
    console.log(list[head]);
    while (head < list.length - 1) {
        let options = [];
        let muts = [];
        let a = list[head];
        let b = list[tail];
        do {
            tail++;
            options.push(b);
            b = list[tail];
        } while (b - a <= buf);
        // console.log("options for", a);
        // console.log(options);
        // console.log(options.length);
        // const isWithin =
        //     list[head - 1] < a //&&
        // largestFromPrev >= options[options.length - 1];
        console.log("options", options);
        console.log("largest option", options[options.length - 1]);
        console.log("last processed", list[head - 1]);
        console.log("largest from last processed", largestFromPrev);
        // console.log("iswithin", isWithin);
        console.log("--------- ");
        if (options.length > 2) {
            largestFromPrev = list[tail - 1];
            let inner = 1;
            let localState = {};
            while (inner < options.length) {
                let parsed = options.filter((num, i) => {
                    return i !== inner || i == options.length - 1;
                });
                if (parsed.length > 1) {
                    let str = parsed.join(".");
                    localState[str] = 1;
                    combos[str] = 1;
                }
                inner++;
            }
            let ops = options[0] + "." + options[options.length - 1];
            localState[ops] = 1;
            muts = Object.keys(localState);
            // muts.push(ops)
            // combos[ops] = 1;
        } else {
            largestFromPrev = -1;
        }
        byA[a] = muts;
        // if(options.length > 2) {
        // 	combos[options.join(".")] = 1;
        // }
        tail = ++head;
    }
    console.log(byA);
    console.log(list);
    return Object.keys(byA).reduce((total, key) => {
        const len = byA[key].length;
        return total + len;
    }, 0);
};
//console.log(part2(example));
// console.log(part2(example2));
// console.log(part2(data));

const part3 = (list) => {
  list = [0, ...list, list[list.length -1] + 3];
  let total = 1;
  let subs = [];
  let i = 0;
  let tail = 0;
  while( i < list.length ) {
    tail = i + 1;
    let sub = [list[i]];
    let next = list[tail];
    while(next - sub[sub.length -1] < 3){
      sub.push(next);
      next = list[++tail]
    }
    subs.push(sub);
    i = i + sub.length;
  }
  let ranges = {}
  subs.forEach(sublist => {
    const sublen = sublist.length;
    if(ranges[sublen]) {
      ranges[sublen].count = ranges[sublen].count + 1
    } else {
      vars = {1: 1, 2: 1, 3: 2, 4: 4, 5: 7}
      ranges[sublen] = {
        variations: vars[sublen],
        count: 1
      }
    }
  })
  console.log(subs)
  console.log(ranges);
  let product = 1;
  for(let [len, item] of Object.entries(ranges)) {
    product = product * (item.variations ** item.count);
  }
  return product;
}
//console.log(part3(example));
console.log(part3(example2));
console.log(part3(data));
