const original = require("./data");
const countOccupiedSeats = (l, x, y) => {
    const N = l[y - 1] && l[y - 1][x];
    const S = l[y + 1] && l[y + 1][x];
    const E = l[y][x + 1] || -1;
    const W = l[y][x - 1] || -1;
    const NE = l[y - 1] && l[y - 1][x + 1];
    const NW = l[y - 1] && l[y - 1][x - 1];
    const SW = l[y + 1] && l[y + 1][x - 1];
    const SE = l[y + 1] && l[y + 1][x + 1];
    const occupied = [N, S, E, W, NE, NW, SW, SE].reduce((occ, seat) => {
        return seat === "#" ? occ + 1 : occ;
    }, 0);
    return occupied;
};
const findInDirection = (l, x, y, dy, dx) => {
    if (!l[y]) {
        return false;
    }
    const bx = x + dx;
    const by = y + dy;
    if (!l[by]) {
        return false;
    }
    const seat = l[by][bx];
    switch (seat) {
        case ".":
            return findInDirection(l, bx, by, dy, dx);
        case "#":
            return ["#"];
        case "L":
            return { x:bx, y: by };
        default:
            return false;
    }
};
const findInDirections = (l, x, y) => {
    const N = [-1, 0];
    const S = [+1, 0];
    const E = [0, +1];
    const W = [0, -1];
    const NE = [-1, +1];
    const NW = [-1, -1];
    const SW = [+1, -1];
    const SE = [+1, +1];
    const directions = [N, S, E, W, NE, NW, SW, SE];
    let occ = 0;
    const free = [];
    for (let dn of directions) {
        let found = findInDirection(l, x, y, dn[0], dn[1]);
        if (found) {
            if (found[0] === "#") {
                occ++;
            } else {
                free.push(found);
            }
        }
    }
    return { free, occupied: occ };
};
const clean = (list) => list.trim().split("\n");
const sample = clean(original);
const example = clean(`
L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL
`);
part1 = (data) => {
    const mut = data.map((row) => {
        return row.split("");
    });
    const isEmpty = (seat) => seat === "L" || seat === ".";
    let changesHaveOccurred = true;
    let seatCount = 0;
    let round = 0;
    let lastSeatCount = -1;
    let lastSet = data.map((row) => row.split(""));
    while (changesHaveOccurred) {
        const inner = lastSet.map((r) => r.map((i) => i));
        changesHaveOccurred = false;
        inner.forEach((row, y) => {
            row.forEach((seat, x) => {
                const occupiedAdj = countOccupiedSeats(lastSet, x, y);
                if (seat !== ".") {
                    if (isEmpty(seat)) {
                        if (occupiedAdj === 0) {
                            changesHaveOccurred = true;
                            seatCount++;
                            inner[y][x] = "#";
                        }
                    } else if (!isEmpty(seat)) {
                        if (occupiedAdj >= 5) {
                            changesHaveOccurred = true;
                            inner[y][x] = "L";
                        } else {
                            seatCount++;
                        }
                    }
                }
            });
        });
        console.log("round", round++, "count", seatCount);
        lastSet = [...inner];
        lastSeatCount = seatCount;
        seatCount = 0;
    }
    return seatCount;
};
// console.log(part1(example))
// console.log(part1(sample))
part2 = (data) => {
    const isEmpty = (seat) => seat === "L";
    let changesHaveOccurred = true;
    let seatCount = 0;
    let round = 0;
    let lastSeatCount = -1;
    let lastSet = data.map((row) => row.split(""));
    while (changesHaveOccurred) {
        // const inner = [...lastSet]
        const inner = lastSet.map((r) => r.map((i) => i));
        // inner.forEach((r) => [console.log(r.join(""))]);
        seatCount = 0;
        changesHaveOccurred = false;
        inner.forEach((row, y) => {
            row.forEach((seat, x) => {
                const { occupied } = findInDirections(lastSet, x, y);
                if (seat !== ".") {
                    if (isEmpty(seat)) {
                        if (occupied === 0) {
                            inner[y][x] = "#";
                            seatCount++;
                            changesHaveOccurred = true;
                        }
                    } else if (seat === "#") {
                        if (occupied >= 5) {
                            changesHaveOccurred = true;
                            inner[y][x] = "L";
                        } else {
                            seatCount++;
                        }
                    }
                }
            });
        });
        console.log("round", round++, "count", seatCount);
        lastSet = inner.map((r) => r.map((i) => i));
        lastSeatCount = seatCount;
    }
    return seatCount;
};
// console.log(part2(example))
console.log(part2(sample))
