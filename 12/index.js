const original = require('./data');
const clean = (l) => l.trim().split('\n')
const example = clean(`
F10
N3
F7
R90
F11
`)
const sample = clean(original);
const directions = ['N', 'E', 'S', 'W']
const part1 = (l) => {
  const state = {ew: 0, ns: 0, facing: 'E'}
  l.forEach(move => {
    const action = move[0];
    const amt = parseInt(move.slice(1));
    //console.log('move:', action);
    //console.log('by:', amt);
    const rotateDegrees = (am) => {
      const currentIndex = directions.indexOf(state.facing);
      const moves = am/90;
      if(moves < 0) {
        // rotating left
        let newIndex = currentIndex + moves < 0
          ? currentIndex + moves + 4
          : currentIndex + moves
        state.facing = directions[newIndex]
      } else {
        // rotating right
        const newIndex = (moves + currentIndex) % 4
        state.facing = directions[newIndex]
      }
    }
    const moveInDirection = (ac, am) => {
      switch(ac) {
        case 'N': 
          state.ns = state.ns + am
          break;
        case 'S':
          state.ns = state.ns - am
          break;
        case 'E':
          state.ew = state.ew + am
          break;
        case 'W':
          state.ew = state.ew - am
          break;
        case 'F': 
          moveInDirection(state.facing, am);
          break;
        case 'L': 
          console.log('turning left by', am)
          rotateDegrees(-1*am)
          break;
        case 'R': 
          console.log('turning right by',am)
          rotateDegrees(am);
          break;
      }
    }
    moveInDirection(action, amt);
    console.log(state);
    console.log('---------');
  })
  console.log(state);
  return Math.abs(state.ew) + Math.abs(state.ns)
}
//console.log(part1(example));
//console.log(part1(sample));
const part2 = (l) => {
  const state = {ew: 0, ns: 0, facing: 'E'}
  const waypoint = {ew: 10, ns: 1} 
  l.forEach(move => {
    const action = move[0];
    const amt = parseInt(move.slice(1));
    const rotateDegrees = (am) => {
      /*
       * ew: 10, ns: 4
       * ew: 4, ns: -10
       * ew: -10: ns: -4
       * ew: -4: ns: 10
      */
      const moves = am/90;

      if(moves < 0) {
        // rotating left
        for(let i = 0; i < Math.abs(moves); i++) {
          const { ew, ns } = waypoint;
          if(ew > 0) {
            waypoint.ew = ns * -1;
            waypoint.ns = ew
          } else {
            waypoint.ew = ns * -1;
            waypoint.ns = ew
          }
        }
      } else {
        // rotating right
        for(let i = 0; i < moves; i++) {
          const { ew, ns } = waypoint;
          if(ew < 0) {
            waypoint.ew = ns;
            waypoint.ns = ew * -1
          } else {
            waypoint.ew = ns;
            waypoint.ns = ew * -1
          }
        }
      }
    }
    const moveInDirection = (ac, am) => {
      switch(ac) {
        case 'N': 
          waypoint.ns = waypoint.ns + am
          break;
        case 'S':
          waypoint.ns = waypoint.ns - am
          break;
        case 'E':
          waypoint.ew = waypoint.ew + am
          break;
        case 'W':
          waypoint.ew = waypoint.ew - am
          break;
        case 'F': 
          state.ew = waypoint.ew * am + state.ew
          state.ns = waypoint.ns * am + state.ns
          break;
        case 'L': 
          console.log('turning left by', am)
          rotateDegrees(-1*am)
          break;
        case 'R': 
          console.log('turning right by',am)
          rotateDegrees(am);
          break;
      }
    }
    moveInDirection(action, amt);
    console.log(state);
    console.log(waypoint);
    console.log('---------');
  })
  console.log(state);
  return Math.abs(state.ew) + Math.abs(state.ns)
}
//console.log(part2(example));
console.log(part2(sample));
