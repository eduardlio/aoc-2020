const scriptFileContents = `
const original = require('./data');
const { cleanData } = require('../utils');

const data = cleanData(original);
const example = cleanData(${"`"}

${"`"});

const part1 = (list) => {

};
console.log(part1(example));
console.log(part1(data));


const part2 = (list) => {

};
console.log(part2(example));
console.log(part2(data));

`;


const dataFileContents = `
module.exports = ${"`"}

${"`"};
`

module.exports = {
  scriptFileContents,
  dataFileContents
}
