#!/usr/bin/node
const fs = require('fs');

const { scriptFileContents, dataFileContents } = require('./contents')

if(process.argv.length !== 3) {
  console.error("Expected only one argument: number from 1 - 25");
  process.exit(1);
  return;
}

const day = parseInt(process.argv[2]);
if(day.toString() == 'NaN' || day < 1 || day > 25){
  console.error("Expected a number between 1 - 25");
  process.exit(1);
  return;
}

const dayStr = parseInt(day) > 10 ? day + "" : "0" + day;
if(fs.existsSync(dayStr)){
  console.error(`Folder ${dayStr} already exists`);
  process.exit(1);
  return;
}

fs.mkdir(dayStr, {}, (err) => {
  if(err) {
    console.warn("Couldn't create folder");
    throw err;
    process.exit(1);
  }

  fs.writeFile(`${dayStr}/data.js`, dataFileContents, 'utf8', (err) => {
    if(err) { 
      console.error("Couldn't create data file");
    } else {
      console.log("Created data file in " + dayStr);
    }
  });
  fs.writeFile(`${dayStr}/index.js`, scriptFileContents, 'utf8', (err) => {
    if(err) { 
      console.error("Couldn't create script file");
    } else {
      console.log("Created script file in " + dayStr);
    }
  });
})


