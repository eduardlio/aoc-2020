let cleanData = source => source.trim().split("\n");
module.exports = {
  cleanData
}
